<?php

/*
*	Global Parameter and functions
*
* 	@version	1.0
* 	@author		Greatives Team
* 	@URI		http://greatives.eu
*/

$movedo_ext_social_list_extended = array (
	array(
		'title' => 'Twitter',
		'url' => 'twitter_url',
		'id' => 'twitter',
		'class' => 'fa fa-twitter',
	),
	array(
		'title' => 'Facebook',
		'url' => 'facebook_url',
		'id' => 'facebook',
		'class' => 'fa fa-facebook',
	),
	array(
		'title' => 'Google+',
		'url' => 'googleplus_url',
		'id' => 'google-plus',
		'class' => 'fa fa-google-plus',
	),
	array(
		'title' => 'Instagram',
		'url' => 'instagram_url',
		'id' => 'instagram',
		'class' => 'fa fa-instagram',
	),
	array(
		'title' => 'LinkedIn',
		'url' => 'linkedin_url',
		'id' => 'linkedin',
		'class' => 'fa fa-linkedin',
	),
	array(
		'title' => 'Tumblr',
		'url' => 'tumblr_url',
		'id' => 'tumblr',
		'class' => 'fa fa-tumblr',
	),
	array(
		'title' => 'Pinterest',
		'url' => 'pinterest_url',
		'id' => 'pinterest',
		'class' => 'fa fa-pinterest',
	),
	array(
		'title' => 'GitHub',
		'url' => 'github_url',
		'id' => 'github',
		'class' => 'fa fa-github',
	),
	array(
		'title' => 'Dribbble',
		'url' => 'dribbble_url',
		'id' => 'dribbble',
		'class' => 'fa fa-dribbble',
	),
	array(
		'title' => 'reddit',
		'url' => 'reddit_url',
		'id' => 'reddit',
		'class' => 'fa fa-reddit',
	),
	array(
		'title' => 'Flickr',
		'url' => 'flickr_url',
		'id' => 'flickr',
		'class' => 'fa fa-flickr',
	),
	array(
		'title' => 'Skype',
		'url' => 'skype_url',
		'id' => 'skype',
		'class' => 'fa fa-skype',
	),
	array(
		'title' => 'YouTube',
		'url' => 'youtube_url',
		'id' => 'youtube',
		'class' => 'fa fa-youtube',
	),
	array(
		'title' => 'Vimeo',
		'url' => 'vimeo_url',
		'id' => 'vimeo',
		'class' => 'fa fa-vimeo',
	),
	array(
		'title' => 'SoundCloud',
		'url' => 'soundcloud_url',
		'id' => 'soundcloud',
		'class' => 'fa fa-soundcloud',
	),
	array(
		'title' => 'WeChat',
		'url' => 'wechat_url',
		'id' => 'wechat',
		'class' => 'fa fa-wechat',
	),
	array(
		'title' => 'Weibo',
		'url' => 'weibo_url',
		'id' => 'weibo',
		'class' => 'fa fa-weibo',
	),
	array(
		'title' => 'Renren',
		'url' => 'renren_url',
		'id' => 'renren',
		'class' => 'fa fa-renren',
	),
	array(
		'title' => 'QQ',
		'url' => 'qq_url',
		'id' => 'qq',
		'class' => 'fa fa-qq',
	),
	array(
		'title' => 'XING',
		'url' => 'xing_url',
		'id' => 'xing',
		'class' => 'fa fa-xing',
	),
	array(
		'title' => 'RSS',
		'url' => 'rss_url',
		'id' => 'rss',
		'class' => 'fa fa-rss',
	),
	array(
		'title' => 'VK',
		'url' => 'vk_url',
		'id' => 'vk',
		'class' => 'fa fa-vk',
	),
	array(
		'title' => 'Behance',
		'url' => 'behance_url',
		'id' => 'behance',
		'class' => 'fa fa-behance',
	),
	array(
		'title' => 'Foursquare',
		'url' => 'foursquare_url',
		'id' => 'foursquare',
		'class' => 'fa fa-foursquare',
	),
	array(
		'title' => 'Steam',
		'url' => 'steam_url',
		'id' => 'steam',
		'class' => 'fa fa-steam',
	),
	array(
		'title' => 'Twitch',
		'url' => 'twitch_url',
		'id' => 'twitch',
		'class' => 'fa fa-twitch',
	),
	array(
		'title' => 'Houzz',
		'url' => 'houzz_url',
		'id' => 'houzz',
		'class' => 'fa fa-houzz',
	),
	array(
		'title' => 'Yelp',
		'url' => 'yelp_url',
		'id' => 'yelp',
		'class' => 'fa fa-yelp',
	),
	array(
		'title' => 'Snapchat',
		'url' => 'snapchat_url',
		'id' => 'snapchat',
		'class' => 'fa fa-snapchat',
	),
	array(
		'title' => 'Medium',
		'url' => 'medium_url',
		'id' => 'medium',
		'class' => 'fa fa-medium',
	),
	array(
		'title' => 'TripAdvisor',
		'url' => 'tripadvisor_url',
		'id' => 'tripadvisor',
		'class' => 'fa fa-tripadvisor',
	),
	array(
		'title' => 'Spotify',
		'url' => 'spotify_url',
		'id' => 'spotify',
		'class' => 'fa fa-spotify',
	),
	array(
		'title' => 'WhatsApp',
		'url' => 'whatsapp_url',
		'id' => 'whatsapp',
		'class' => 'fa fa-whatsapp',
	),
	array(
		'title' => 'Telegram',
		'url' => 'telegram_url',
		'id' => 'telegram',
		'class' => 'fa fa-telegram',
	),
);

/**
 * Get instagram array
 */
function movedo_ext_get_instagram_array( $username, $limit, $order_by, $order, $cache = "", $access_token = "", $user_id = ""  ) {

	$username = strtolower( $username );
	$transient_string = $access_token .'-'. $username .'-'. $order_by .'-'. $order;

	if ( false === ( $instagram = get_transient('grve-instagram-feed-v2-'.sanitize_title_with_dashes( $transient_string ) ) ) || empty( $cache ) ) {

		if( !empty( $user_id ) && !empty( $access_token ) ) {
			$url = 'https://api.instagram.com/v1/users/' . $user_id . '/media/recent/?access_token=' . $access_token;
		} else {
			$url = 'https://www.instagram.com/' . $username . '/media/';
		}

		$remote = wp_remote_get( esc_url( $url ) );

		if ( is_wp_error( $remote ) ) {
			if( current_user_can( 'administrator' ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram!', 'movedo' ) );
			} else {
				return new WP_Error('site_down', '' );
			}
		}
		if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
			if( current_user_can( 'administrator' ) ) {
				return new WP_Error('invalid_response', esc_html__( 'Instagram invalid response!', 'movedo' ) );
			} else {
				return new WP_Error('invalid_response', '' );
			}
		}

		$insta_array = json_decode( $remote['body'], TRUE );

		if( !empty( $user_id ) && !empty( $access_token ) ) {
			$images = isset( $insta_array['data'] ) ? $insta_array['data'] : array();
		} else {
			$images = isset( $insta_array['items'] ) ? $insta_array['items'] : array();
		}

		$instagram = array();

		foreach ( $images as $image ) {

			if ($image['user']['username'] == $username) {

				$image['link']                          = preg_replace( "/^http:/i", "", $image['link'] );
				$image['images']['thumbnail']           = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
				$image['images']['low_resolution']      = preg_replace( "/^http:/i", "", $image['images']['low_resolution'] );
				$image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );

				$instagram[] = array(
					'description'   => $image['caption']['text'],
					'link'          => $image['link'],
					'time'          => $image['created_time'],
					'comments'      => $image['comments']['count'],
					'likes'         => $image['likes']['count'],
					'thumbnail'     => $image['images']['thumbnail'],
					'medium'        => $image['images']['low_resolution'],
					'large'         => $image['images']['standard_resolution'],
					'type'          => $image['type']
				);
			}
		}

		//Instagram Order
		if ( 'none' != $order_by ) {
			foreach ($instagram as $key => $row) {
				$time[$key] = $row['time'];
				$comments[$key]  = $row['comments'];
				$likes[$key] = $row['likes'];
			}
			if ( 'ASC' == $order ) {
				$order = SORT_ASC;
			} else {
				$order = SORT_DESC;
			}
			if ( 'datetime' == $order_by ) {
				$order_by = $time;
			} elseif ( 'comments' == $order_by ) {
				$order_by = $comments;
			} elseif ( 'likes' == $order_by ) {
				$order_by = $likes;
			}
			array_multisort( $order_by, $order, $instagram );
		}

		if( !empty( $cache ) ) {
			set_transient('grve-instagram-feed-v2-'.sanitize_title_with_dashes( $transient_string ), $instagram, apply_filters( 'movedo_ext_instagram_cache_time', HOUR_IN_SECONDS ) );
		}
	}

	return array_slice( $instagram, 0, $limit );
}

function movedo_ext_print_post_bg_image( $image_size = 'movedo-grve-fullscreen' ) {
	if ( has_post_thumbnail() ) {
		$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, $image_size );
		$image_url = $attachment_src[0];
?>
		<div class="grve-bg-image" style="background-image: url(<?php echo esc_url( $image_url ); ?>);"></div>
<?php
	}
}

 function movedo_ext_print_select_options( $selector_array, $current_value = "" ) {

	foreach ( $selector_array as $value=>$display_value ) {
	?>
		<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $current_value, $value ); ?>><?php echo esc_html( $display_value ); ?></option>
	<?php
	}

}