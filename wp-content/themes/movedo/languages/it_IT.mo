��    /      �  C                   &  
   3     >     B     K     O     f     s     �     �     �     �     �     �     �     �     �     �            	   1     ;     C     H     Y     w  
   ~     �     �     �  O   �  E        R     b     j     s     y     �     �  $   �     �     �  	   �  	   �     �     �     
     /
     ?
     N
     T
     \
     `
     w
     �
     �
     �
     �
     �
     �
     �
     �
          ,     4     K     h  	   {     �     �     �  )   �  	   �     �     �  	          P   +  Q   |     �     �  	   �  	   �     �     
       ,   &     S     U     X     f     |                                     '               -   *            !          $              "                    %   
      )                             	          .   ,   &         #              /             +   (                     %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Hours Leave a Reply Leave a Reply to Link Relationship (XFN) Log out Log out of this account Logged in as Monthly Archives : Move down Move up Name Navigation Label Open link in a new window/tab Phone: Posts By : Search Results for : Search for ... Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. Title Attribute Website Website: Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. at in logged in read more to post a comment. Project-Id-Version: Movedo v2.6
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-22 16:48+0200
PO-Revision-Date: 2019-02-22 16:48+0200
Last-Translator: 
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
X-Poedit-SearchPathExcluded-1: includes/framework
X-Poedit-SearchPathExcluded-2: tribe-events
 %s (non valido) %s (in sospeso) (obbligatorio) Tutto Archivi Di: Classi CSS (opzionale) Cancella la risposta I commenti sono chiusi. Archivi del giorno : Giorni Email Modifica elemento del menu Ore Lascia una risposta Lascia una risposta a Relazione Collegamenti (XFN) Log out Esci da questo account Hai effettuato il login come Archivi del mese : Muovi giu Muovi Sopra Nome Etichetta di navigazione Apri il link in una nuova finestra/scheda Telefono: Articolo di : Risultati della ricerca per : Cerca ... Invia il commento La descrizione sarà mostrata nel menu se il tema supporta questa funzionalità. Questo post è protetto da password. Inserisci la password per vedere i commenti. Attributo Titolo Sito Sito web: Settimane Archivi annuali : Devi Il tuo commento qui… Il tuo commento è in attesa di moderazione. a in fare il login maggiori informazioni per poter inviare un commento. 