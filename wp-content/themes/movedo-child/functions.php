<?php

/**
 * Child Theme
 *
 */

/**
 * Include helper files
 */
require_once get_template_directory() . '-child' . '/includes/grve-header-functions.php';

/**
 * Theme setup function
 * Theme support
 */
function movedo_child_theme_setup() {

}
add_action( 'after_setup_theme', 'movedo_child_theme_setup' );

/**
 * Theme translation
 */
function movedo_child_theme_locale() {
    load_child_theme_textdomain( 'movedochild', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'movedo_child_theme_locale' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
